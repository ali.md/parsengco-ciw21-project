<!DOCTYPE html>
<!--[if lt IE 7]>		<html lang="fa-IR" dir="rtl" class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>			<html lang="fa-IR" dir="rtl" class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>			<html lang="fa-IR" dir="rtl" class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->	<html lang="fa-IR" dir="rtl" class="no-ie"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<title>شرکت مهندسی پارس | <?php echo $template['title'] ?></title>
	<base href="<?php echo $template['url']; ?>" />
	<link rel="stylesheet" type="text/css" href="1styles.css" />
	<link rel="stylesheet" type="text/css" href="style.css" />
	<script type="text/javascript" src="./scripts/html5shiv.js"></script>
	<link href='http://fonts.googleapis.com/css?family=Pacifico|PT+Sans+Narrow' rel='stylesheet' type='text/css' />
</head>
<body>
<!--[if lt IE 9]>
	<p class="chromeframe"><br/>برای مشاهده صحیح این وب سایت مرورگر خود را <a href="http://browsehappy.com/" target="_blank"><b>به روز رسانی</b></a> نموده و یا <a href="http://www.chromeframe.ir/" target="_blank"><b>این پلاگین</b></a> را نصب نمایید.<br/><br/></p><br/>
<![endif]-->